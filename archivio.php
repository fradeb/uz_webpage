<!DOCTYPE html>
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <meta name="Author" content="Made by 'tree'">
 <meta name="GENERATOR" content="$Version: $ tree v1.8.0 (c) 1996 - 2018 by Steve Baker, Thomas Moore, Francesc Rocher, Florian Sesser, Kyosuke Tokoro $">
 <title>Archivio</title>
 <style type="text/css">
  <!-- 
  BODY { font-family : ariel, monospace, sans-serif; }
  P { font-weight: normal; font-family : ariel, monospace, sans-serif; color: black; background-color: transparent;}
  B { font-weight: normal; color: black; background-color: transparent;}
  A:visited { font-weight : normal; text-decoration : none; background-color : transparent; margin : 0px 0px 0px 0px; padding : 0px 0px 0px 0px; display: inline; }
  A:link    { font-weight : normal; text-decoration : none; margin : 0px 0px 0px 0px; padding : 0px 0px 0px 0px; display: inline; }
  A:hover   { color : #000000; font-weight : normal; text-decoration : underline; background-color : yellow; margin : 0px 0px 0px 0px; padding : 0px 0px 0px 0px; display: inline; }
  A:active  { color : #000000; font-weight: normal; background-color : transparent; margin : 0px 0px 0px 0px; padding : 0px 0px 0px 0px; display: inline; }
  .VERSION { font-size: small; font-family : arial, sans-serif; }
  .NORM  { color: black;  background-color: transparent;}
  .FIFO  { color: purple; background-color: transparent;}
  .CHAR  { color: yellow; background-color: transparent;}
  .DIR   { color: blue;   background-color: transparent;}
  .BLOCK { color: yellow; background-color: transparent;}
  .LINK  { color: aqua;   background-color: transparent;}
  .SOCK  { color: fuchsia;background-color: transparent;}
  .EXEC  { color: green;  background-color: transparent;}
  -->
 </style>
</head>
<body>
	<h1>Archivio</h1><p>
	<a class="NORM" href="/~fradeb/archivio">/~fradeb/archivio</a><br>
	├── <a class="DIR" href="/~fradeb/archivio/sns/">sns</a><br>
	│   └── <a class="DIR" href="/~fradeb/archivio/sns/secondo_anno/">secondo_anno</a><br>
	│   &nbsp;&nbsp;&nbsp; └── <a class="DIR" href="/~fradeb/archivio/sns/secondo_anno/marmi/">marmi</a><br>
	│   &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; ├── <a class="DIR" href="/~fradeb/archivio/sns/secondo_anno/marmi/lezioni/">lezioni</a><br>
	│   &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; └── <a class="DIR" href="/~fradeb/archivio/sns/secondo_anno/marmi/livieri/">livieri</a><br>
	│   &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; ├── <a class="DIR" href="/~fradeb/archivio/sns/secondo_anno/marmi/livieri/esercizi/">esercizi</a><br>
	│   &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; └── <a class="DIR" href="/~fradeb/archivio/sns/secondo_anno/marmi/livieri/lezioni/">lezioni</a><br>
	└── <a class="DIR" href="/~fradeb/archivio/unipi/">unipi</a><br>
	&nbsp;&nbsp;&nbsp; ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/">secondo_anno</a><br>
	&nbsp;&nbsp;&nbsp; │   ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/anaghisi/">anaghisi</a><br>
	&nbsp;&nbsp;&nbsp; │   │   ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/anaghisi/compiti/">compiti</a><br>
	&nbsp;&nbsp;&nbsp; │   │   └── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/anaghisi/lezioni/">lezioni</a><br>
	&nbsp;&nbsp;&nbsp; │   ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/chimica/">chimica</a><br>
	&nbsp;&nbsp;&nbsp; │   │   ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/chimica/equilibrio/">equilibrio</a><br>
	&nbsp;&nbsp;&nbsp; │   │   ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/chimica/esercizi/">esercizi</a><br>
	&nbsp;&nbsp;&nbsp; │   │   ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/chimica/itinere/">itinere</a><br>
	&nbsp;&nbsp;&nbsp; │   │   ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/chimica/struttura_atomica/">struttura_atomica</a><br>
	&nbsp;&nbsp;&nbsp; │   │   └── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/chimica/termochimica/">termochimica</a><br>
	&nbsp;&nbsp;&nbsp; │   ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/fisica2/">fisica2</a><br>
	&nbsp;&nbsp;&nbsp; │   │   ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/fisica2/ciampini/">ciampini</a><br>
	&nbsp;&nbsp;&nbsp; │   │   │   └── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/fisica2/ciampini/lezioni/">lezioni</a><br>
	&nbsp;&nbsp;&nbsp; │   │   │   &nbsp;&nbsp;&nbsp; ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/fisica2/ciampini/lezioni/primo_semestre/">primo_semestre</a><br>
	&nbsp;&nbsp;&nbsp; │   │   │   &nbsp;&nbsp;&nbsp; └── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/fisica2/ciampini/lezioni/secondo_semestre/">secondo_semestre</a><br>
	&nbsp;&nbsp;&nbsp; │   │   ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/fisica2/macchi/">macchi</a><br>
	&nbsp;&nbsp;&nbsp; │   │   │   ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/fisica2/macchi/primo_semestre/">primo_semestre</a><br>
	&nbsp;&nbsp;&nbsp; │   │   │   └── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/fisica2/macchi/secondo_semestre/">secondo_semestre</a><br>
	&nbsp;&nbsp;&nbsp; │   │   └── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/fisica2/strumia/">strumia</a><br>
	&nbsp;&nbsp;&nbsp; │   ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/lab/">lab</a><br>
	&nbsp;&nbsp;&nbsp; │   │   ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/lab/dispense_fuso/">dispense_fuso</a><br>
	&nbsp;&nbsp;&nbsp; │   │   ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/lab/lavagnate/">lavagnate</a><br>
	&nbsp;&nbsp;&nbsp; │   │   └── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/lab/manuali/">manuali</a><br>
	&nbsp;&nbsp;&nbsp; │   ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/meccanica/">meccanica</a><br>
	&nbsp;&nbsp;&nbsp; │   │   ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/meccanica/compiti_ma/">compiti_ma</a><br>
	&nbsp;&nbsp;&nbsp; │   │   └── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/meccanica/compiti_mc/">compiti_mc</a><br>
	&nbsp;&nbsp;&nbsp; │   └── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/metodi1/">metodi1</a><br>
	&nbsp;&nbsp;&nbsp; │   &nbsp;&nbsp;&nbsp; ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/metodi1/altro/">altro</a><br>
	&nbsp;&nbsp;&nbsp; │   &nbsp;&nbsp;&nbsp; ├── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/metodi1/bolognesi/">bolognesi</a><br>
	&nbsp;&nbsp;&nbsp; │   &nbsp;&nbsp;&nbsp; └── <a class="DIR" href="/~fradeb/archivio/unipi/secondo_anno/metodi1/esami/">esami</a><br>
	&nbsp;&nbsp;&nbsp; └── <a class="DIR" href="/~fradeb/archivio/unipi/terzo_anno/">terzo_anno</a><br>
	&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; ├── <a class="DIR" href="/~fradeb/archivio/unipi/terzo_anno/gd/">gd</a><br>
	&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; │   └── <a class="DIR" href="/~fradeb/archivio/unipi/terzo_anno/gd/salvetti_slides/">salvetti_slides</a><br>
	&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; └── <a class="DIR" href="/~fradeb/archivio/unipi/terzo_anno/metodi2/">metodi2</a><br>
	&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; ├── <a class="DIR" href="/~fradeb/archivio/unipi/terzo_anno/metodi2/altro/">altro</a><br>
	&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; ├── <a class="DIR" href="/~fradeb/archivio/unipi/terzo_anno/metodi2/compiti/">compiti</a><br>
	&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; │   └── <a class="DIR" href="/~fradeb/archivio/unipi/terzo_anno/metodi2/compiti/nuovi/">nuovi</a><br>
	&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; │   &nbsp;&nbsp;&nbsp; ├── <a class="DIR" href="/~fradeb/archivio/unipi/terzo_anno/metodi2/compiti/nuovi/soluzioni/">soluzioni</a><br>
	&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; │   &nbsp;&nbsp;&nbsp; └── <a class="DIR" href="/~fradeb/archivio/unipi/terzo_anno/metodi2/compiti/nuovi/testi/">testi</a><br>
	&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; └── <a class="DIR" href="/~fradeb/archivio/unipi/terzo_anno/metodi2/lezioni/">lezioni</a><br>
	<br><br>
	</p>
	<p>


</body>
</html>

